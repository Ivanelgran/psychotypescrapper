import time
import csv
import json
import pandas as pd

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import *
from bs4 import BeautifulSoup
from datetime import datetime
from multiprocessing import Pool, Queue, Manager
from functools import partial

username = "+36203751496"
password = "insta2019"
promocode = "PsyData"

option = Options()
# option.add_argument('headless')
# option.add_argument("--disable-infobars")
# option.add_argument("start-maximized")
# option.add_argument("--disable-extensions")
option.add_experimental_option("prefs", {
    "profile.default_content_setting_values.notifications": 1
})

driver = webdriver.Chrome(chrome_options=option)


def df_auth():
    try:
        driver.get('http://vk.datafuel.ru/login')
        time.sleep(10)
        driver.find_element_by_partial_link_text(
            "Войти через ВКонтакте").click()
        time.sleep(3)
        return True
    except Exception as e:
        print(e)
        return False

# with open('text.html', "w", encoding="utf-8") as f:
#     f.write(driver.page_source)


def df_balance_checker():
    info = []
    for elem in driver.find_elements_by_css_selector("li#demo-px-nav-box>div"):
        info.append(elem.text)
    user = str(info[0][18:])
    requests_balance = int(info[2])
    export_balance = int(info[4])
    print(
        f"Available balance of requests for the user {user} - {requests_balance}, export balance - {export_balance}")

    return user, requests_balance, export_balance


def df_promo_checker():
    promo = driver.find_elements_by_css_selector("input.ng-valid")
    promo[0].send_keys(promocode)
    button = driver.find_elements_by_css_selector("button.action-button")
    button[0].click()
    time.sleep(3)
    # print(driver.find_element_by_xpath("/html/body[@class='px-responsive-bg-container']/my-app/div/master-page/div[@id='toast-container']/div[@class='toast toast-success toast-error']"))
    # print(driver.find_element_by_partial_link_text("для вашего аккаунта уже был активирован промо код, свяжитесь с поддержкой, если вы еше не активировали промо код"))
    # print(driver.find_element_by_xpath("//strong[contains(text(),'для вашего аккаунта уже был активирован промо код')]"))



def df_manager(q, file_name):
    if not df_auth():
        print("Datafuel authorization failed")
        return
    df_promo_checker()
    user, requests_balance, export_balance = df_balance_checker()
    if requests_balance > 0:
        df_task_manager(user, q, file_name, requests_balance)


def df_task_manager(user, q, file_name, requests_balance):
    tasks = []
    driver.get('http://vk.datafuel.ru/analisys')
    name_form = driver.find_elements_by_css_selector("input.ng-pristine")
    link_form = driver.find_elements_by_css_selector(
        "textarea.ng-pristine")
    analyze_button = driver.find_elements_by_css_selector(
        "button.action-button")
    for i in range(int(requests_balance)):
        if not q.empty():
            link = q.get()
            task_name = str(link) + '_task'
            name_form[0].send_keys(task_name)
            link_form[0].send_keys('https://vk.com/id' + str(link))
            analyze_button[1].click()
            tasks.append(task_name)
            time.sleep(2)
        else:
            break

    df_results_table_parser(tasks, user, file_name)


def df_results_table_parser(tasks, user, file_name):
    driver.get('http://vk.datafuel.ru/result-lists')
    table_page = driver.page_source
    soup_table_page = BeautifulSoup(table_page, 'html.parser')
    result_table = json.loads(soup_table_page.find('pre').contents[0])
    psychotype_handler(tasks, result_table, user, file_name)


def psychotype_handler(tasks, result_table, user, file_name):
    result_tasks = []
    for name in tasks:
        result_tasks += list(
            filter(lambda person: person['Name'] == name, result_table))
    error_list = list(filter(
        lambda person: person['Ready'] == True and person['Broken'] == True, result_tasks))
    ready_list = list(filter(
        lambda person: person['Ready'] == True and person['Broken'] == False, result_tasks))
    waiting_list = list(filter(
        lambda person: person['Ready'] == False and person['Broken'] == False, result_tasks))
    print(
        f"User {str(user)}: Ready tasks - {str(len(ready_list))}, tasks pending - {str(len(waiting_list))}, tasks completed with an error - {str(len(error_list))}")

    i = 0
    if len(ready_list) > 0:
        for psy_req in ready_list:
            json_result = get_result(str(psy_req['Id']))
            stat_to_csv(json_result, file_name)
            i += 1
        ready_list.clear()
    while len(waiting_list) != 0:
        for pend_task in waiting_list:
            json_result = get_result(str(pend_task['Id']))
            if not json_result['Result'] == None:
                waiting_list[:] = [d for d in waiting_list if d.get(
                    'Id') != str(pend_task['Id'])]
                stat_to_csv(json_result, file_name)
                i += 1
        print(
            f"User {str(user)}: Number of tasks pending - {str(len(waiting_list))}")
        time.sleep(2)
    print(f"User {str(user)}: Number of completed tasks - {str(i)}, tasks completed with an error - {str(len(error_list))}")


def stat_to_csv(result, file_name):
    result_list = []
    result_dict = {}
    age_dict = result['Result']['bday_stat']['stats']['distribution']
    result_dict['age'] = get_stat_from_dict(age_dict)
    sex = result['Result']['sex_stat']['stats']
    result_dict['sex'] = get_stat_from_dict(sex)
    result_dict['vk_id'] = result['Name'].split('_task')[0]
    result_dict['city'] = list(
        result['Result']['city_stat'].get('stats')[0].keys())[0] or None

    values_to_list = ['life_main_stat',
                      'people_main_stat', 'relation_stat', 'mbti_stat']
    for value in values_to_list:
        stat = result['Result'][value]['stats']
        result_dict[value] = get_stat_from_list(stat)
    result_list.append(result_dict)
    df = pd.DataFrame(result_list)
    df.to_csv(f'../output/{file_name}', index=False, mode='a', header=False, columns=[
              'vk_id', 'age', 'sex', 'city', 'life_main_stat', 'people_main_stat', 'relation_stat', 'mbti_stat'])


def get_result(id):
    req_link = 'http://vk.datafuel.ru/result?id=' + str(id)
    driver.get(req_link)
    page = driver.page_source
    soup_page = BeautifulSoup(page, 'html.parser')
    json_result = json.loads(soup_page.find('pre').contents[0])
    return json_result


def get_stat_from_list(stat_list):
    is_stat = None
    for stat_dict in stat_list:
        if is_stat != None:
            break
        for key in stat_dict.keys():
            if stat_dict[str(key)] == 1:
                is_stat = key
                break
            else:
                is_stat = None
    return is_stat


def get_stat_from_dict(stat_dict):
    is_stat = None
    for key in stat_dict.keys():
        if stat_dict[str(key)] == 1:
            is_stat = key
            break
        else:
            is_stat = None
    return is_stat


def vk_auth(username, password):
    for i in range(0,3):
        try:
            driver.get('http://vk.com')
            login = driver.find_element_by_id("index_email")
            login.send_keys(username)

            pswd = driver.find_element_by_id("index_pass")
            pswd.send_keys(password)

            driver.find_element_by_id("index_login_button").click()
            time.sleep(10)
            driver.find_element_by_id("top_logout_link")

        # except NoSuchElementException:
        #     print("Authorization failed")
        #     time.sleep(5)
        #     continue

        except Exception as e:
            print("An exception has occurred:")
            print(e)
            continue

        else:
            return True

def parser_start(q, file_name, bots_data):
    if vk_auth(bots_data['login'], bots_data['password']):
        df_manager(q, file_name)


def main():
    with open ('../input/vk_data.json', 'r') as f:
        vk_ids = list(json.loads(f.read()).keys())
        id_count = len(vk_ids)
    with open('../input/vk_bots.json', 'r') as f:
        vk_bots = json.loads(f.read())
    dt_string = datetime.now().strftime("%d_%m_%Y__%H_%M_%S")
    file_name = str(dt_string) + '_' + str(id_count) + '.csv'
    new_df = pd.DataFrame(columns=['vk_id', 'age', 'sex', 'city',
                                   'life_main_stat', 'people_main_stat', 'relation_stat', 'mbti_stat'])
    new_df.to_csv(f'../output/{file_name}', index=False, mode='w')
    m = Manager()
    q_id = m.Queue()
    for vk_id in vk_ids:
        q_id.put(vk_id)
    func = partial(parser_start, q_id, file_name)
    pool = Pool(processes=1)
    pool.map(func, vk_bots)
    pool.close()
    pool.join()


if __name__ == '__main__':
    main()
